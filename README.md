# ansible guile install role
Install guile on debian systems using ansible

## Getting Started
You might want to change the variables `guile_sources_path_root` or `guile_install_path` in your playbook depending on your preferences.  
By default the install root dir is ~/bin/. So your `guile` binary will be in ~/bin/bin/. Override it in your playbook/role if necessary.

### Prerequisites
- A debian system.
- Ansible.

### How to run
If you're just installing in a local path (on your home dir), then

```
ansible-playbook your_playbook_including_this_role.yml
```
will do.  
If you are installing guile to a system path (that would require root access), then add "-b" at the end.

# Motivation
guile is not really hard to install. But so are lots of things that depend on it (lately I've been playing around with the web framework "artanis" that runs on guile). This role can be a base or a dependency for those hairy ones.

# This is not github did you notice?
Yeah, totally. Because, f*ck microsoft.  
I think someday ansible galaxy will accept other repos besides github. Until then, just clone this repo and use it directly (can't use ansible galaxy).

